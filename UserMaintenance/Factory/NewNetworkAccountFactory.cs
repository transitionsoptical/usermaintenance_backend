﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NewNetworkAccountFactory : RequestFormFactory
    {
        public NewNetworkAccountForm NewNetworkAccount { get; set; }

        public NewNetworkAccountFactory(UserRequestFormViewModel viewModel)
            : base(viewModel)
        {
            NewNetworkAccount = new NewNetworkAccountForm(viewModel.NewAccountUserInfoViewModel);
        }

        public override UserRequestForm CreateUserRequestForm()
        {
            var userRequestForm = new UserRequestForm(MainForm, NewNetworkAccount);

            return userRequestForm;
        }
    }
}