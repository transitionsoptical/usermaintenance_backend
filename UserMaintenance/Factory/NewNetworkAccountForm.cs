﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Factory
{
    public class NewNetworkAccountForm
    {
        public NewNetworkAccountForm(Models.NewAccountUserInfoViewModel viewModel)
        {
            EmployeeFirstName = viewModel.EmployeeFirstName;
            EmployeeMiddleName = viewModel.EmployeeMiddleName;
            EmployeeLastName = viewModel.EmployeeLastName;
            JobTitle = viewModel.JobTitle;
            HasExternalAccess = viewModel.HasExternalAccess;
            NewLaptopIsRequired = viewModel.NewLaptopIsRequired;
            EmploymentType = viewModel.EmploymentType;
            InternetAccessGroup = viewModel.InternetAccessGroup;
            UserLocation = viewModel.UserLocation;
            UserToCopyFirstName = viewModel.UserToCopyFirstName;
            UserToCopyLastName = viewModel.UserToCopyLastName;
            AccountStartDate = viewModel.AccountStartDate;
            EmailDistribution = viewModel.EmailDistribution;
            SecurityGroup = viewModel.SecurityGroup;
            AccessToApplication = viewModel.AccessToApplication;
        }

        public string EmployeeFirstName { get; set; }

        public string EmployeeMiddleName { get; set; }

        public string EmployeeLastName { get; set; }

        public string JobTitle { get; set; }

        public bool HasExternalAccess { get; set; }

        public bool NewLaptopIsRequired { get; set; }

        public string EmploymentType { get; set; }

        public string InternetAccessGroup { get; set; }

        public string UserLocation { get; set; }

        public string UserToCopyFirstName { get; set; }

        public string UserToCopyLastName { get; set; }

        public string AccountStartDate { get; set; }

        //Other Applications / Security Groups/ Distribution Groups
        public string EmailDistribution { get; set; }

        public string SecurityGroup { get; set; }

        public string AccessToApplication { get; set; }
    }
}