﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NewServiceAccountForm
    {
        public NewServiceAccountForm(NewServiceAccountViewModel viewModel)
        {
            Systems = viewModel.Systems;
            Purpose = viewModel.Purpose;
            DateRequired = viewModel.DateRequired;
            LocationService = viewModel.LocationService;
        }

        public string Systems { get; set; }

        public string Purpose { get; set; }

        public DateTime DateRequired { get; set; }

        public string LocationService { get; set; }
    }
}