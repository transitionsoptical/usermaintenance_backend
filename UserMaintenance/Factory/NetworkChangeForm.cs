﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NetworkChangeForm
    {
        public NetworkChangeForm(NetworkChangeViewModel viewModel)
        {
            AccountName = viewModel.AccountName;
            RequestDetails = viewModel.RequestDetails;
            UserLocation = viewModel.UserLocation;
        }

        public string AccountName { get; set; }

        public string RequestDetails { get; set; }

        public string UserLocation { get; set; }
    }
}