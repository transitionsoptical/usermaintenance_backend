﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class MainForm
    {
        private MainForm() { }

        public MainForm(UserRequestFormViewModel viewModel)
        {
            RequesterFirstName = viewModel.RequesterFirstName;
            RequesterLastName = viewModel.RequesterLastName;
            RequesterPhone = viewModel.RequesterPhone;
            RequesterEmail = viewModel.RequesterEmail;
            ManagerFirstName = viewModel.ManagerFirstName;
            ManagerLastName = viewModel.ManagerLastName;
            ManagerEmail = viewModel.ManagerEmail;
        }

        public string RequesterFirstName { get; set; }

        public string RequesterLastName { get; set; }

        public string RequesterPhone { get; set; }

        public string RequesterEmail { get; set; }

        public string ManagerFirstName { get; set; }

        public string ManagerLastName { get; set; }

        public string ManagerEmail { get; set; }
    }
}