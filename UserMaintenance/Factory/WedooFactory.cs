﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class WedooFactory : RequestFormFactory
    {
        public WedooForm WedooForm { get; set; }

        public WedooFactory(UserRequestFormViewModel viewModel) : base(viewModel)
        {
            WedooForm = new WedooForm(viewModel.WedooViewModel);
        }

        public override UserRequestForm CreateUserRequestForm()
        {
            var userRequestForm = new UserRequestForm(MainForm, WedooForm);

            return userRequestForm;
        }
    }
}