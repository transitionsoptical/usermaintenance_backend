﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NetworkRemovalForm
    {
        public NetworkRemovalForm(NetworkRemovalViewModel viewModel)
        {
            AccountNameForRemoval = viewModel.AccountNameForRemoval;
            NeedToRetainData = viewModel.NeedToRetainData;
            NetworkRemovalDetails = viewModel.NetworkRemovalDetails;
        }

        public string AccountNameForRemoval { get; set; }

        public bool NeedToRetainData { get; set; }

        public string NetworkRemovalDetails { get; set; }
    }
}