﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NetworkRemovalFactory : RequestFormFactory
    {
        public NetworkRemovalForm NetworkRemovalForm { get; set; }

        public NetworkRemovalFactory(UserRequestFormViewModel viewModel) 
            : base(viewModel)
        {
            NetworkRemovalForm = new NetworkRemovalForm(viewModel.NetworkRemovalViewModel);
        }

        public override UserRequestForm CreateUserRequestForm()
        {
            var userRequestForm = new UserRequestForm(MainForm, NetworkRemovalForm);

            return userRequestForm;
        }
    }
}