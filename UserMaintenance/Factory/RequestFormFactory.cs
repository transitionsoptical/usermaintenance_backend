﻿using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public abstract class RequestFormFactory
    {
        public MainForm MainForm { get; set; }


        public RequestFormFactory(UserRequestFormViewModel viewModel)
        {
            MainForm = new MainForm(viewModel);
        }

        public abstract UserRequestForm CreateUserRequestForm();
    }
}