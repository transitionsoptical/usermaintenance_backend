﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NetworkChangeFactory : RequestFormFactory
    {
        public NetworkChangeForm NetworkChangeForm { get; set; }

        public NetworkChangeFactory(UserRequestFormViewModel viewModel) 
            : base(viewModel)
        {
            NetworkChangeForm = new NetworkChangeForm(viewModel.NetworkChangeViewModel);
        }

        public override UserRequestForm CreateUserRequestForm()
        {
            var userRequestForm = new UserRequestForm(MainForm, NetworkChangeForm);

            return userRequestForm;
        }
    }
}