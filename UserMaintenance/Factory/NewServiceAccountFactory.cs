﻿using UserMaintenance.Entities;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class NewServiceAccountFactory : RequestFormFactory
    {
        public NewServiceAccountForm NewServiceAccount { get; set; }

        public NewServiceAccountFactory(UserRequestFormViewModel viewModel) 
            : base(viewModel)
        {
            NewServiceAccount = new NewServiceAccountForm(viewModel.NewServiceAccountViewModel);
        }

        public override UserRequestForm CreateUserRequestForm()
        {
            var userRequestForm = new UserRequestForm(MainForm, NewServiceAccount);

            return userRequestForm;
        }
    }
}