﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Models;

namespace UserMaintenance.Factory
{
    public class WedooForm
    {
        public WedooForm(WedooViewModel viewModel)
        {
            EmailAccountType = viewModel.EmailAccountType;
            AccountToChange = viewModel.AccountToChange;
            ChangeDetails = viewModel.ChangeDetails;
            MeetingRoomName = viewModel.MeetingRoomName;
            MeetingRoomRemovalDetails = viewModel.MeetingRoomRemovalDetails;
            WedooGroupRequest = viewModel.WedooGroupRequest;
            GroupNameToAdd = viewModel.GroupNameToAdd;
            OwnersOfTheGroup = viewModel.OwnersOfTheGroup;
            MembersOfTheGroup = viewModel.MembersOfTheGroup;
            GroupNameToRemove = viewModel.GroupNameToRemove;
            GroupRemovalDetails = viewModel.GroupRemovalDetails;
        }

        //Add Email Account
        public string EmailAccountType { get; set; }

        //Change Wedoo email account access or settings
        public string AccountToChange { get; set; }

        public string ChangeDetails { get; set; }

        //Wedoo - Change or Remove a Calendar Meeting Room
        public string MeetingRoomName { get; set; }

        public string MeetingRoomRemovalDetails { get; set; }

        //Add/Change/Remove email group
        public string WedooGroupRequest { get; set; }

        //Add
        public string GroupNameToAdd { get; set; }

        public string OwnersOfTheGroup { get; set; }

        public string MembersOfTheGroup { get; set; }

        //Change or Remove
        public string GroupNameToRemove { get; set; }

        public string GroupRemovalDetails { get; set; }
    }
}