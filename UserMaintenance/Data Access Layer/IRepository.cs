﻿using UserMaintenance.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserMaintenance.Data_Access_Layer
{
    public interface IRepository<T>
        where T: IEntity
    {
        T GetById(Guid masterId);

        void Save(T entity);

        void Update(T entity);
    }
}
