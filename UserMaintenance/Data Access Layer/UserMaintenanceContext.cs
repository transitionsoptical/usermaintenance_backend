﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UserMaintenance.Entities;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace UserMaintenance.Data_Access_Layer
{
    public class UserMaintenanceContext: DbContext
    {
        public UserMaintenanceContext() : base("UserMaintenanceContext")
        {
        }

        public DbSet<UserRequestForm> UserRequestForm { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}