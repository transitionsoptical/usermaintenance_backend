﻿using UserMaintenance.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UserMaintenance.Data_Access_Layer
{
    public class UserMaintenanceRepository
    {
        private readonly UserMaintenanceContext _context;

        public UserMaintenanceRepository(UserMaintenanceContext context)
        {
            _context = context;
        }

        public UserRequestForm GetById(int id)
        {
            var entity = _context.UserRequestForm
                .Where(x => x.RequestId == id)
                .FirstOrDefault();

            return entity;
        }

        public void Update(UserRequestForm entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChangesAsync();
        }

        internal void Save(UserRequestForm userRequestForm)
        {
            _context.UserRequestForm.Add(userRequestForm);
            _context.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}