﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class InternalAccessGroups
    {
        public const string Basic = "Basic";

        public const string Enhanced = "Enhanced";

        public const string Exec = "Exec";
    }
}