﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class WedooOptions
    {
        public const string AddEmailAccount = "Add email account (User, Application or Shared)";

        public const string ChangeEmailAccount = "Change email Account access or settings";

        public const string AddChangeRemoveEmailGroup = "Add/Change/Remove email group";

        public const string AddCalendarMeetingRoom = "Add Calendar Meeting Room";

        public const string ChangeRemoveCalendarMeetingRoom = "Wedoo - Change or Remove a Calendar Meeting Room";

        public const string Skip = "Skip this section - No Wedoo account required";

        //Wedoo Group
        public const string AddWedooGroup = "Add a Wedoo Group";

        public const string ChangeOrRemoveWedooGroup = "Change or Remove a Wedoo Group";
    }
}