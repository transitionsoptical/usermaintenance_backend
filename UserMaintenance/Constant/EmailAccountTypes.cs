﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class EmailAccountTypes
    {
        public const string EmployeeContractor = "Employee/Contractor";

        public const string ApplicationMailbox = "Application Mailbox";

        public const string SharedMailbox = "Shared Mailbox";
    }
}