﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class Applications
    {
        public const string MFGPro = "MFGPro";

        public const string Corvu = "Corvu";

        public const string SAS = "SAS";

        public const string CRM = "CRM";

        //Transitions World Access
        
        public const string TOLENGINEERING = "TOLENGINEERING Security Group";

        public const string TOLLAB = "TOLLAB Security Group";

        public const string TOLMARKETING = "TOLMARKETING Security Group";

        public const string TOLANALYTICSRD = "TOLANALYTICSRD Security Groups";

        public const string All = "* All Transitions(Global) Distribution group";
    }
}