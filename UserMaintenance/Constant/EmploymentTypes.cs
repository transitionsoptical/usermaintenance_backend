﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class EmploymentTypes
    {
        public const string Employee = "Employee";

        public const string Contractor = "Contractor";
    }
}