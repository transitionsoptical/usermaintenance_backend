﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class AccountType
    {
        public const string User = "User";

        public const string ServiceAccount = "Service Account";

        public const string RoomType = "Room Type";
    }
}