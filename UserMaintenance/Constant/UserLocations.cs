﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constant
{
    public class UserLocations
    {
        public const string TOB = "TOB";

        public const string TOI = "TOI";

        public const string TOL = "TOL";

        public const string TOPI = "TOPI";

        public const string TOTL = "TOTL";

        public const string ESSPI = "ESSPI";

        public const string Paris = "TOB";

        public const string Singapore = "TOB";

        public const string Australia = "Australia";

        public const string Other = "Other";
    }
}