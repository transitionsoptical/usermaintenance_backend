﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Constants
{
    public class NetworkAccountRequestTypes
    {
        public const string AddNew = "Add New Network Account";

        public const string ChangeExisting = "Change Existing Network Account";

        public const string RemoveExisting = "Remove Existing Network Account";

        public const string Wedoo = "Wedoo Account(No AD account required)";

        public const string SharedMailbox = "Shared Mailbox/Email Distribution Group";
    }
}