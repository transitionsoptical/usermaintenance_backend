﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Service
{
    public class TransactionResult
    {
        public TransactionResult()
        {
            IsSuccess = true;
        }

        public TransactionResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }
}