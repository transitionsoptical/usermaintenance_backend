﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserMaintenance.Data_Access_Layer;
using UserMaintenance.Entities;
using UserMaintenance.Factory;
using UserMaintenance.Models;

namespace UserMaintenance.Service
{
    public class UserMaintenanceService
    {
        private RequestFormFactory _factory; 

        private readonly UserMaintenanceContext _context;

        private readonly UserMaintenanceRepository _repository;

        public UserMaintenanceService()
        {
            _context = new UserMaintenanceContext();
            _repository = new UserMaintenanceRepository(_context);
        }

        public TransactionResult InsertUserRequest(UserRequestFormViewModel viewModel)
        {
            try
            {
                MapFactory(viewModel);
                var userRequestForm = _factory.CreateUserRequestForm();

                _repository.Save(userRequestForm);

                return new TransactionResult();
            }
            catch (Exception ex)
            {
                return new TransactionResult(ex.Message);
            }
        }

        private void MapFactory(UserRequestFormViewModel viewModel)
        {
            if (viewModel.NetworkAccountRequestCategory == Constant.NetworkAccountRequestCategories.AddNew)
            {
                if (viewModel.AccountType == Constant.AccountType.User)
                {
                    _factory = new NewNetworkAccountFactory(viewModel);
                }
                else if (viewModel.AccountType == Constant.AccountType.ServiceAccount)
                {
                    _factory = new NewServiceAccountFactory(viewModel);
                }
            }
            else if(viewModel.NetworkAccountRequestCategory == Constant.NetworkAccountRequestCategories.ChangeExisting) 
            {
                _factory = new NetworkChangeFactory(viewModel);
            }
            else if (viewModel.NetworkAccountRequestCategory == Constant.NetworkAccountRequestCategories.RemoveExisting)
            {
                _factory = new NetworkRemovalFactory(viewModel);
            }
            else //Wedoo
            {
                _factory = new WedooFactory(viewModel);
            }
        }
    }
}