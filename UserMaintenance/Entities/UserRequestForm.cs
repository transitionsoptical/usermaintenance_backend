﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using UserMaintenance.Factory;
using UserMaintenance.Models;

namespace UserMaintenance.Entities
{
    public class UserRequestForm
    {
        private WedooForm wedooForm;

        public UserRequestForm() { }

        public UserRequestForm(MainForm mainForm)
        {
            RequesterFirstName = mainForm.RequesterFirstName;
            RequesterLastName = mainForm.RequesterLastName;
            RequesterEmail = mainForm.RequesterEmail;
            RequesterPhone = mainForm.RequesterPhone;
            ManagerFirstName = mainForm.ManagerFirstName;
            ManagerLastName = mainForm.ManagerLastName;
            ManagerEmail = mainForm.ManagerEmail;
        }

        public UserRequestForm(MainForm mainForm, NewNetworkAccountForm form)
            :this(mainForm)
        {
            EmployeeFirstName = form.EmployeeFirstName;
            EmployeeMiddleName = form.EmployeeMiddleName;
            EmployeeLastName = form.EmployeeLastName;
            JobTitle = form.JobTitle;
            HasExternalAccess = form.HasExternalAccess;
            NewLaptopIsRequired = form.NewLaptopIsRequired;
            EmploymentType = form.EmploymentType;
            InternetAccessGroup = form.InternetAccessGroup;
            UserLocation = form.UserLocation;
            UserToCopyFirstName = form.UserToCopyFirstName;
            UserToCopyLastName = form.UserToCopyLastName;
            AccountStartDate = form.AccountStartDate;
            EmailDistribution = form.EmailDistribution;
            SecurityGroup = form.SecurityGroup;
            AccessToApplication = form.AccessToApplication;
        }

        public UserRequestForm(MainForm mainForm, NewServiceAccountForm form)
            :this(mainForm)
        {
            Systems = form.Systems;
            Purpose = form.Purpose;
            DateRequired = form.DateRequired;
            LocationService = form.LocationService;
        }

        public UserRequestForm(MainForm mainForm, NetworkChangeForm form) 
            : this(mainForm)
        {
            AccountName = form.AccountName;
            RequestDetails = form.RequestDetails;
            NetworkChangeUserLocation = form.UserLocation;
        }

        public UserRequestForm(MainForm mainForm, NetworkRemovalForm form) 
            : this(mainForm)
        {
            AccountNameForRemoval = form.AccountNameForRemoval;
            NeedToRetainData = form.NeedToRetainData;
            NetworkRemovalDetails = form.NetworkRemovalDetails;
        }

        public UserRequestForm(MainForm mainForm, WedooForm form) 
            : this(mainForm)
        {
            EmailAccountType = form.EmailAccountType;
            AccountToChange = form.AccountToChange;
            ChangeDetails = form.ChangeDetails;
            MeetingRoomName = form.MeetingRoomName;
            MeetingRoomRemovalDetails = form.MeetingRoomRemovalDetails;
            WedooGroupRequest = form.WedooGroupRequest;
            GroupNameToAdd = form.GroupNameToAdd;
            OwnersOfTheGroup = form.OwnersOfTheGroup;
            MembersOfTheGroup = form.MembersOfTheGroup;
            GroupNameToRemove = form.GroupNameToRemove;
            GroupRemovalDetails = form.GroupRemovalDetails;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RequestId { get; private set; }

        [Required]
        public string RequesterFirstName { get; private set; }

        [Required]
        public string RequesterLastName { get; private set; }

        [Required]
        public string RequesterEmail { get; private set; }

        [Required]
        public string RequesterPhone { get; private set; }

        [Required]
        public string ManagerFirstName { get; private set; }

        [Required]
        public string ManagerLastName { get; private set; }

        [Required]
        public string ManagerEmail { get; private set; }

        //New User Network Account
        public string EmployeeFirstName { get; private set; }

        public string EmployeeMiddleName { get; private set; }

        public string EmployeeLastName { get; private set; }

        public string JobTitle { get; private set; }

        public bool HasExternalAccess { get; private set; }

        public bool NewLaptopIsRequired { get; private set; }

        public string EmploymentType { get; private set; }

        public string InternetAccessGroup { get; private set; }

        public string UserLocation { get; private set; }

        public string UserToCopyFirstName { get; private set; }

        public string UserToCopyLastName { get; private set; }

        public string AccountStartDate { get; private set; }

        public string EmailDistribution { get; private set; }

        public string SecurityGroup { get; private set; }

        public string AccessToApplication { get; private set; }
        public string Systems { get; private set; }

        public string Purpose { get; private set; }

        public DateTime DateRequired { get; private set; }

        public string LocationService { get; private set; }

        public string AccountName { get; private set; }

        public string RequestDetails { get; private set; }

        public string NetworkChangeUserLocation { get; private set; }

        public string AccountNameForRemoval { get; private set; }

        public bool NeedToRetainData { get; private set; }

        public string NetworkRemovalDetails { get; private set; }

        public string EmailAccountType { get; private set; }

        public string AccountToChange { get; private set; }

        public string ChangeDetails { get; private set; }

        public string MeetingRoomName { get; private set; }

        public string MeetingRoomRemovalDetails { get; private set; }

        public string WedooGroupRequest { get; private set; }

        public string GroupNameToAdd { get; private set; }

        public string OwnersOfTheGroup { get; private set; }

        public string MembersOfTheGroup { get; private set; }

        public string GroupNameToRemove { get; private set; }

        public string GroupRemovalDetails { get; }
    }
}