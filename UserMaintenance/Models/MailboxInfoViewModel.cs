﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserMaintenance.Models
{
    public class MailboxInfoViewModel
    {
        [Display(Name="Systems")]
        public string SharedMailboxSystems { get; set; }

        [Display(Name = "Purpose")]
        public string SharedMailboxPurpose { get; set; }

        [Display(Name = "Proposed Account Name")]
        public string SharedMailboxProposedAccountName { get; set; }

        public bool ApplicationWillDirectlyLoginToThisAccount { get; set; }

        [Display(Name = "Estimated amount of storage required")]
        public string SharedMailboxEstimatedStorage { get; set; }

        [Display(Name = "Who will be the owner")]
        public string SharedMailboxOwner { get; set; }

        [Display(Name = "Location")]

        public string SharedMailboxLocation { get; set; }

        [Display(Name = "Expiration Date if needed")]
        public DateTime SharedMailboxExpirationDate { get; set; }
    }
}