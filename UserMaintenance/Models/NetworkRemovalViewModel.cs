﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Models
{
    public class NetworkRemovalViewModel
    {
        public string AccountNameForRemoval { get; set; }

        public bool NeedToRetainData { get; set; }

        public string NetworkRemovalDetails { get; set; }
    }
}