﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserMaintenance.Models
{
    public class ErrorPageViewModel
    {
        public ErrorPageViewModel() { }

        public ErrorPageViewModel(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage { get; protected set; }
    }
}