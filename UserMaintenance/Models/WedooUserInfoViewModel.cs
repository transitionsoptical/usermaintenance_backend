﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Models
{
    public class WedooUserInfoViewModel
    {
        public List<SelectListItem> _accountUsageOptions;

        public WedooUserInfoViewModel()
        {
            AccountUsageOptions = new List<SelectListItem>();
        }

        [Display(Name = "Name")]
        public string WedooUserFirstName { get; set; }

        public string WedooUserMiddleName { get; set; }

        public string WedooUserLastName { get; set; }

        [Display(Name = "Department")]
        public string WedooUserDepartment { get; set; }

        [Display(Name = "Job Title")]
        public string WedooUserJobTitle { get; set; }

        [Display(Name = "Phone")]
        public string WedooUserPhone { get; set; }

        [Display(Name = "Employee Type")]
        public string WedooUserEmploymentType { get; set; }

        [Display(Name = "Reporting Supervisor Name")]
        public string WedooUserReportingSupervisorName { get; set; }

        [Display(Name = "Reporting Supervisor Email")]
        public string WedooUserReportingSupervisorEmailAddress { get; set; }

        [Display(Name = "Location")]
        public string WedooUserLocation { get; set; }

        [Display(Name = "Are there any other instructions needed for this user?")]
        public string InstructionDetails { get; set; }

        [Display(Name = "Contractor Start Date")]
        public DateTime ContractorStartDate { get; set; }

        public string AccountUsage { get; set; }

        public IEnumerable<SelectListItem> AccountUsageOptions
        {
            get { return _accountUsageOptions; }
            set
            {
                _accountUsageOptions = new List<SelectListItem>()
                {
                    new SelectListItem { Value = "Production(15G Storage)", Text = "Production(15G Storage)" },
                    new SelectListItem { Value = "Standard(30G Storage)", Text = "Standard(30G Storage)" }
                };
            }
        }
    }
}