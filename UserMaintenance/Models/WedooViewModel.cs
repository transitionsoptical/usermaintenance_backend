﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Models
{
    public class WedooViewModel
    {
        private List<SelectListItem> _emailAccountTypes;

        private List<SelectListItem> _wedooGroupRequestOptions;

        private List<SelectListItem> _wedooOptions;

        public WedooViewModel()
        {
            EmailAccountTypes = new List<SelectListItem>();
            WedooGroupRequestOptions = new List<SelectListItem>();
            WedooOptions = new List<SelectListItem>();
            WedooUserInfoViewModel = new WedooUserInfoViewModel();
            MailboxInfoViewModel = new MailboxInfoViewModel();
        }

        //Add Email Account
        public string EmailAccountType { get; set; }

        public List<SelectListItem> EmailAccountTypes
        {
            get { return _emailAccountTypes; }
            set
            {
                _emailAccountTypes = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.EmailAccountTypes.EmployeeContractor, Text = Constant.EmailAccountTypes.EmployeeContractor },
                    new SelectListItem { Value = Constant.EmailAccountTypes.ApplicationMailbox, Text = Constant.EmailAccountTypes.ApplicationMailbox},
                    new SelectListItem { Value = Constant.EmailAccountTypes.SharedMailbox, Text = Constant.EmailAccountTypes.SharedMailbox}
                };
            }
        }

        public WedooUserInfoViewModel WedooUserInfoViewModel { get; set; }

        public MailboxInfoViewModel MailboxInfoViewModel { get; set; }


        //Change Wedoo email account access or settings
        [Display(Name="What account do you wish to change")]
        public string AccountToChange { get; set; }

        [Display(Name ="Detail What needs to be changed")]
        public string ChangeDetails { get; set; }

        //Wedoo - Change or Remove a Calendar Meeting Room
        [Display(Name = "What is the name of the meeting room?")]
        public string MeetingRoomName { get; set; }

        [Display(Name = "Please detail what you would like done")]
        public string MeetingRoomRemovalDetails { get; set; }

        //Add/Change/Remove email group
        public string WedooGroupRequest { get; set; }

        public List<SelectListItem> WedooGroupRequestOptions
        {
            get { return _wedooGroupRequestOptions; }
            set
            {

                _wedooGroupRequestOptions = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.WedooOptions.AddWedooGroup, Text = Constant.WedooOptions.AddWedooGroup },
                    new SelectListItem { Value = Constant.WedooOptions.ChangeOrRemoveWedooGroup, Text = Constant.WedooOptions.ChangeOrRemoveWedooGroup}
                };
            }
        }

        public string Wedoo { get; set; }

        
        public List<SelectListItem> WedooOptions
        {
            get { return _wedooOptions; }
            set
            {
                _wedooOptions = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.WedooOptions.AddEmailAccount, Text = Constant.WedooOptions.AddEmailAccount },
                    new SelectListItem { Value = Constant.WedooOptions.ChangeEmailAccount, Text = Constant.WedooOptions.ChangeEmailAccount},
                    new SelectListItem { Value = Constant.WedooOptions.AddChangeRemoveEmailGroup, Text = Constant.WedooOptions.AddChangeRemoveEmailGroup},
                    new SelectListItem { Value = Constant.WedooOptions.AddCalendarMeetingRoom, Text = Constant.WedooOptions.AddCalendarMeetingRoom},
                    new SelectListItem { Value = Constant.WedooOptions.ChangeRemoveCalendarMeetingRoom, Text = Constant.WedooOptions.ChangeRemoveCalendarMeetingRoom},
                    new SelectListItem { Value = Constant.WedooOptions.Skip, Text = Constant.WedooOptions.Skip}
                };
            }
        }

        //Wedoo Group
        //Add
        [Display(Name ="What is the name of the Group you want to add")]
        public string GroupNameToAdd { get; set; }

        [Display(Name = "Owners of the group")]
        public string OwnersOfTheGroup { get; set; }

        [Display(Name = "Members of the group")]
        public string MembersOfTheGroup { get; set; }

        //Change or Remove
        [Display(Name = "What is the Group you want to change or remove?")]
        public string GroupNameToRemove { get; set; }

        [Display(Name = "Please detail what you would like done to this group")]
        public string GroupRemovalDetails { get; set; }
    }
}