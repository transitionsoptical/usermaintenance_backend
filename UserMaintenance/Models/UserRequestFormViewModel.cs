﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UserMaintenance.Constant;

namespace UserMaintenance.Models
{
    public class UserRequestFormViewModel
    {
        private List<string> _networkAccountRequestCategories;

        private List<string> _accountTypes;

        public UserRequestFormViewModel()
        {
            NewAccountUserInfoViewModel = new NewAccountUserInfoViewModel();
            NewServiceAccountViewModel = new NewServiceAccountViewModel();
            NetworkChangeViewModel = new NetworkChangeViewModel();
            NetworkRemovalViewModel = new NetworkRemovalViewModel();
            WedooViewModel = new WedooViewModel();

            NetworkAccountRequestCategories = new List<string>();
            AccountTypes = new List<string>();
        }

        [Required(ErrorMessage = "Requester Name is required")]
        [Display(Name = "Requester Name: *")]
        public string RequesterFirstName { get; set; }

        [Required(ErrorMessage = "Requester Name is required")]
        [Display(Name = "Requester Name: *")]
        public string RequesterLastName { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [Display(Name = "Requester Phone: *")]
        public string RequesterPhone { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [Display(Name = "Requester Email: *")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string RequesterEmail { get; set; }

        [Required(ErrorMessage = "Manager Name is required")]
        [Display(Name = "Manager Name: *")]
        public string ManagerFirstName { get; set; }

        [Required(ErrorMessage = "Manager Name is required")]
        public string ManagerLastName { get; set; }

        [Required(ErrorMessage = "Manager Email is required")]
        [Display(Name = "Manager Email: *")]
        public string ManagerEmail { get; set; }

        public string NetworkAccountRequestCategory { get; set; }

        public List<string> NetworkAccountRequestCategories
        {
            get { return _networkAccountRequestCategories; }
            set
            {
                _networkAccountRequestCategories = new List<string>()
                {
                    Constant.NetworkAccountRequestCategories.AddNew,
                    Constant.NetworkAccountRequestCategories.ChangeExisting,
                    Constant.NetworkAccountRequestCategories.RemoveExisting,
                    Constant.NetworkAccountRequestCategories.Wedoo,
                    Constant.NetworkAccountRequestCategories.SharedMailbox
                };
            }
        }

        public string AccountType { get; set; }

        public List<string> AccountTypes
        {
            get { return _accountTypes; }
            set
            {
                _accountTypes = new List<string>()
                {
                    Constant.AccountType.User,
                    Constant.AccountType.ServiceAccount,
                    Constant.AccountType.RoomType
                };
            }
        }

        public NewAccountUserInfoViewModel NewAccountUserInfoViewModel { get; set; }

        public NewServiceAccountViewModel NewServiceAccountViewModel { get; set; }

        public NetworkChangeViewModel NetworkChangeViewModel { get; set; }

        public NetworkRemovalViewModel NetworkRemovalViewModel { get; set; }

        public WedooViewModel WedooViewModel { get; set; }
    }
}