﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Models
{
    public class NewServiceAccountViewModel
    {
        private List<SelectListItem> _locationServices;

        public NewServiceAccountViewModel()
        {
            LocationServices = new List<SelectListItem>();
        }

        public string Systems { get; set; }

        [Display(Name="What is the purpose of this account")]
        public string Purpose { get; set; }

        [Display(Name ="Date Required:")]
        public DateTime DateRequired { get; set; }

        public string LocationService { get; set; }

        public List<SelectListItem> LocationServices
        {
            get { return _locationServices; }
            set
            {
                _locationServices = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.UserLocations.TOB, Text = Constant.UserLocations.TOB } ,
                    new SelectListItem { Value = Constant.UserLocations.TOI, Text = Constant.UserLocations.TOI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOL, Text = Constant.UserLocations.TOL },
                    new SelectListItem { Value = Constant.UserLocations.TOPI, Text = Constant.UserLocations.TOPI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOTL, Text = Constant.UserLocations.TOTL } ,
                    new SelectListItem { Value = Constant.UserLocations.ESSPI, Text = Constant.UserLocations.ESSPI } ,
                    new SelectListItem { Value = Constant.UserLocations.Paris, Text = Constant.UserLocations.Paris } ,
                    new SelectListItem { Value = Constant.UserLocations.Singapore, Text = Constant.UserLocations.Singapore } ,
                    new SelectListItem { Value = Constant.UserLocations.Australia, Text = Constant.UserLocations.Australia } ,
                    new SelectListItem { Value = Constant.UserLocations.Other, Text = Constant.UserLocations.Other }
                };
            }
        }
    }
}