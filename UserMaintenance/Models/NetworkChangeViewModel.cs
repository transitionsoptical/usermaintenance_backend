﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Models
{
    public class NetworkChangeViewModel
    {
        private List<SelectListItem> _userLocations;

        public NetworkChangeViewModel()
        {
            UserLocations = new List<SelectListItem>();
        }
        public string AccountName { get; set; }

        public string RequestDetails { get; set; }

        public string UserLocation { get; set; }

        public List<SelectListItem> UserLocations
        {
            get { return _userLocations; }
            set
            {
                _userLocations = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.UserLocations.TOB, Text = Constant.UserLocations.TOB } ,
                    new SelectListItem { Value = Constant.UserLocations.TOI, Text = Constant.UserLocations.TOI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOL, Text = Constant.UserLocations.TOL },
                    new SelectListItem { Value = Constant.UserLocations.TOPI, Text = Constant.UserLocations.TOPI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOTL, Text = Constant.UserLocations.TOTL } ,
                    new SelectListItem { Value = Constant.UserLocations.ESSPI, Text = Constant.UserLocations.ESSPI } ,
                    new SelectListItem { Value = Constant.UserLocations.Paris, Text = Constant.UserLocations.Paris } ,
                    new SelectListItem { Value = Constant.UserLocations.Singapore, Text = Constant.UserLocations.Singapore } ,
                    new SelectListItem { Value = Constant.UserLocations.Australia, Text = Constant.UserLocations.Australia } ,
                    new SelectListItem { Value = Constant.UserLocations.Other, Text = Constant.UserLocations.Other }
                };
            }
        }
    }
}