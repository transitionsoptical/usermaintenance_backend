﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Models
{
    public class NewAccountUserInfoViewModel
    {
        private List<string> _employmentTypes;

        private List<SelectListItem> _internetAccessGroups;

        private List<SelectListItem> _userLocations;

        private List<SelectListItem> _accessToApplications;

        public NewAccountUserInfoViewModel()
        {
            EmploymentTypes = new List<string>();
            InternetAccessGroups = new List<SelectListItem>();
            UserLocations = new List<SelectListItem>();
            AccessToApplications = new List<SelectListItem>();
        }

        [Display(Name = "Employee Name:")]
        public string EmployeeFirstName { get; set; }

        public string EmployeeMiddleName { get; set; }

        public string EmployeeLastName { get; set; }

        [Display(Name = "Job Title:")]
        public string JobTitle { get; set; }

        public bool HasExternalAccess { get; set; }

        public bool NewLaptopIsRequired { get; set; }

        public string EmploymentType { get; set; }

        public List<string> EmploymentTypes
        {
            get { return _employmentTypes; }
            set
            {
                _employmentTypes = new List<string>()
                {
                    Constant.EmploymentTypes.Employee,
                    Constant.EmploymentTypes.Contractor
                };
            }
        }

        public string InternetAccessGroup { get; set; }

        public IEnumerable<SelectListItem> InternetAccessGroups
        {
            get { return _internetAccessGroups; }
            set
            {
                _internetAccessGroups = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.InternalAccessGroups.Basic, Text = Constant.InternalAccessGroups.Basic},
                    new SelectListItem { Value = Constant.InternalAccessGroups.Basic, Text = Constant.InternalAccessGroups.Enhanced},
                    new SelectListItem { Value = Constant.InternalAccessGroups.Basic, Text = Constant.InternalAccessGroups.Exec}
                };
            }
        }

        public string UserLocation { get; set; }

        public List<SelectListItem> UserLocations
        {
            get { return _userLocations; }
            set
            {
                _userLocations = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.UserLocations.TOB, Text = Constant.UserLocations.TOB } ,
                    new SelectListItem { Value = Constant.UserLocations.TOI, Text = Constant.UserLocations.TOI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOL, Text = Constant.UserLocations.TOL },
                    new SelectListItem { Value = Constant.UserLocations.TOPI, Text = Constant.UserLocations.TOPI } ,
                    new SelectListItem { Value = Constant.UserLocations.TOTL, Text = Constant.UserLocations.TOTL } ,
                    new SelectListItem { Value = Constant.UserLocations.ESSPI, Text = Constant.UserLocations.ESSPI } ,
                    new SelectListItem { Value = Constant.UserLocations.Paris, Text = Constant.UserLocations.Paris } ,
                    new SelectListItem { Value = Constant.UserLocations.Singapore, Text = Constant.UserLocations.Singapore } ,
                    new SelectListItem { Value = Constant.UserLocations.Australia, Text = Constant.UserLocations.Australia } ,
                    new SelectListItem { Value = Constant.UserLocations.Other, Text = Constant.UserLocations.Other }
                };
            }
        }

        [Display(Name = "User To Copy")]
        public string UserToCopyFirstName { get; set; }

        public string UserToCopyLastName { get; set; }

        [Display(Name = "Account Start Date:")]
        public string AccountStartDate { get; set; }

        //Other Applications / Security Groups/ Distribution Groups
        public string EmailDistribution { get; set; }

        public string SecurityGroup { get; set; }

        public string AccessToApplication { get; set; }

        public List<SelectListItem> AccessToApplications
        {
            get { return _accessToApplications; }
            set
            {
                _accessToApplications = new List<SelectListItem>()
                {
                    new SelectListItem { Value = Constant.Applications.MFGPro, Text = Constant.Applications.MFGPro } ,
                    new SelectListItem { Value = Constant.Applications.Corvu, Text = Constant.Applications.Corvu } ,
                    new SelectListItem { Value = Constant.Applications.SAS, Text = Constant.Applications.SAS },
                    new SelectListItem { Value = Constant.Applications.SAS, Text = Constant.Applications.CRM}
                };
            }
        }
    }
}