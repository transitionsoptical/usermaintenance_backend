﻿using UserMaintenance.Data_Access_Layer;
using UserMaintenance.Models;
using UserMaintenance.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace UserMaintenance.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserMaintenanceService _userMaintenanceService;

        public HomeController()
        {
            _userMaintenanceService = new UserMaintenanceService();
            
        }

        public ActionResult Index()
        {
            var userRequestFormViewModel = new UserRequestFormViewModel();
            return View(userRequestFormViewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserRequestFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var validationResult = _userMaintenanceService.InsertUserRequest(viewModel);
                if (!validationResult.IsSuccess)
                {
                    return View("~/Views/Shared/Error.cshtml", new ErrorPageViewModel(validationResult.ErrorMessage));
                }

                return View("~/Views/Shared/SuccessPageViewModel.cshtml", new SuccessPageViewModel());
            }

            return View();
        }
    }
}